const express = require('express');
const app = express();

const {
  PORT = "3030"
} = process.env;
const path = require("path");

// "expose" css, javascript and pictures so the html can find it.
app.use('/assets', express.static(path.join(__dirname, 'public', 'static')));
//app.use(express.json());
// app.use(express.urlencoded({extended : true}));

// return navigatio page
app.get("/", (req, res) => res.status(200).sendFile(path.join(__dirname, 'public', 'html', 'index.html')));

//return pure json
app.get("/data", (req, res) => res.status(200).sendFile(path.join(__dirname, 'data.json')));

//return the superb resturnats page
app.get("/resturants", (req, res) => res.status(200).sendFile(path.join(__dirname, 'public', 'html', "resturants.html")));

app.listen(PORT, () => console.log(`Server started on ${PORT}`));
