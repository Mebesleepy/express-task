fetch('/data')
  .then(res => res.json())
  .then((res) => {
    res.forEach((resturant, i) => {

      let resturantMarkup = `
          <div class="resturant">
          <p>Name: ${resturant.name}</p>
          <p>Location: ${resturant.location}</p>
          <p>description: ${resturant.description}</p>
          <p>Average Score: ${resturant.averageScore}</p>
          <image src="/assets/resources/${resturant.image}" alt="resturant image">
          </div>
          <h3>Reviews</h3>
          `;

      resturant.reviews.forEach((review) => {
        resturantMarkup += `
            <div class="review">
              <p>User-name: ${review.userName}</p>
              <p>User score: ${review.score}</p>
              <p>User review ${review.review}</p>
            </div>
            `;
      });
      document.body.innerHTML += resturantMarkup;
    });
  });
